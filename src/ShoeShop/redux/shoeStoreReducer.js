import { dataShoes } from "../DataShoes";
import { ADD_TO_CART, VIEW_DETAIL } from "./constant/shoeStoreConstant";
let initialState = {
  shoeArr: dataShoes,
  detail: dataShoes[0],
  cart: [],
};
export const shoeStoreReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart }; //cú pháp es6 - Ta return state, đồng thời update cart rỗng thành cloneCart
    }
    case VIEW_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
