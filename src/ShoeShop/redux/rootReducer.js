import { combineReducers } from "redux";
import { shoeStoreReducer } from "./shoeStoreReducer";

export const rootReducer = combineReducers({
  shoeStoreReducer,
});
