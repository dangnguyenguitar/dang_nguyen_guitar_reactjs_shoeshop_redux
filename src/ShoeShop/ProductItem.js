import React, { Component } from "react";
import { act } from "react-dom/test-utils";
import { connect } from "react-redux";
class ProductItem extends Component {
  render() {
    let { name, price, shortDescription, image } = this.props.data;
    return (
      <div className="col-3 p-2">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{shortDescription}</p>
            <p className="card-text">Price: ${price}</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-dark"
            >
              Add to card <i class="fa fa-cart-plus"></i>
            </button>
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
              className="btn btn-info ml-2"
              data-toggle="modal"
              data-target="#exampleModalCenter"
            >
              Details
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      let action = {
        type: "ADD_TO_CART",
        payload: shoe,
      };
      dispatch(action);
    },
    handleViewDetail: (shoe) => {
      let action = {
        type: "VIEW_DETAIL",
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ProductItem);
