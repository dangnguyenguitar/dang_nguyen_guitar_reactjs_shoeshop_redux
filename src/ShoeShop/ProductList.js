import React, { Component } from "react";
import ProductItem from "./ProductItem";
import { connect } from "react-redux";
class ProductList extends Component {
  renderProductList = () => {
    return this.props.danhSachGiay.map((item) => {
      return <ProductItem data={item} />;
    });
  };

  render() {
    return this.renderProductList();
  }
}
let mapStateToProps = (state) => {
  return {
    danhSachGiay: state.shoeStoreReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ProductList);
