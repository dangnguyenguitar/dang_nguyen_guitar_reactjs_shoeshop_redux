import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoes } from "./DataShoes";
import DetailShoe from "./DetailShoe";
import Header from "./Header";
import ProductList from "./ProductList";
export default class ShoeShop extends Component {
  render() {
    return (
      <div className="container">
        <Cart />
        <Header />
        <div className="row mx-auto">
          <ProductList />
        </div>
        <DetailShoe />
      </div>
    );
  }
}
